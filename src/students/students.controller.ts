import {Body, Controller, Get, HttpCode, Param, ParseIntPipe, Post, Render, Res} from '@nestjs/common';
import {Response} from "express";
import {StudentsService} from "./students.service";
import {Student} from "./students.model";

// controller for the path /students

@Controller('students')
export class StudentsController {
    constructor(private readonly studentsService: StudentsService) {};

    // routes to /students an uses the StudentService to provide data about all students;
    // @Render shows path to template
    @Get()
    @Render('students')
    async findAll(): Promise<Object> {
        const students = await this.studentsService.findAll();
        return {students};
    }

    // routes to /students/0 for example
    @Get(':matriculationNumber')
    async find(@Param('matriculationNumber', new ParseIntPipe()) matriculationNumber): Promise<Student> {
        return await this.studentsService.find(matriculationNumber);
    }

    // method to add a student;
    // also listening to /students;
    // when using a REST-client to test, remember to choose JSON-format
    @Post()
    @HttpCode(201)
    async create(@Body() student: Partial<Student>, @Res() res: Response) {
        await this.studentsService.addStudent(student);

        res.append('Location', '/students/' + student.matriculationNumber);
        res.redirect('students');
    }
}

// functions as a model-student, so controller and service agree on what a student is;
// normally would need to fit with a database

export interface Student {

    matriculationNumber: number;
    name: string;

}

import {HttpModule, Module} from '@nestjs/common';
import { StudentsService } from './students.service';
import { StudentsController } from './students.controller';

// basis to provide students;
// HttpModule (not HttpService) has to be set as import;

@Module({
  imports: [HttpModule],
  providers: [StudentsService],
  controllers: [StudentsController]
})
export class StudentsModule {}

import { NestFactory } from '@nestjs/core';
import {NestExpressApplication} from "@nestjs/platform-express";
import {join} from 'path';
import { AppModule } from './app.module';

// creates new Nest app and defines port

// modifications for handlebars and express
const PUBLIC_Path = join(__dirname, '..', 'public');
const VIEWS_Path = join(__dirname, '..', 'views');

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // hbs is the handlebars wrapper provided by express
  app.useStaticAssets(PUBLIC_Path);
  app.setBaseViewsDir(VIEWS_Path);
  app.setViewEngine('hbs');

  await app.listen(3000);
}

bootstrap();

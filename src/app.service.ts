import { Injectable } from '@nestjs/common';

// provides functionality in the app;
// '@Injectable' makes it available as a dependency in other classes,
// therefore it has to be enlisted in AppModule

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
